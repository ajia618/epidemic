       
var mnb = document.getElementById("container").getAttribute("src");
// 省份模拟数据
dataSource = {
    '北京': [
        {id: 1, title: '北京人力资源和社会保障局', create_time: '2020-02-24'},
        {id: 2, title: '北京卫健委', create_time: '2020-02-24'},
        {id: 3, title: '北京教育局', create_time: '2020-02-24'},
        {id: 1, title: '北京人力资源和社会保障局', create_time: '2020-02-24'},
        {id: 2, title: '北京卫健委', create_time: '2020-02-24'},
        {id: 3, title: '北京教育局', create_time: '2020-02-24'},
        {id: 1, title: '北京人力资源和社会保障局', create_time: '2020-02-24'},
        {id: 2, title: '北京卫健委', create_time: '2020-02-24'},
        {id: 3, title: '北京教育局', create_time: '2020-02-24'},
        {id: 1, title: '北京人力资源和社会保障局', create_time: '2020-02-24'},
        {id: 2, title: '北京卫健委', create_time: '2020-02-24'},
        {id: 3, title: '北京教育局', create_time: '2020-02-24'},
        {id: 1, title: '北京人力资源和社会保障局', create_time: '2020-02-24'},
        {id: 2, title: '北京卫健委', create_time: '2020-02-24'},
        {id: 3, title: '北京教育局', create_time: '2020-02-24'},
        {id: 1, title: '北京人力资源和社会保障局', create_time: '2020-02-24'},
        {id: 2, title: '北京卫健委', create_time: '2020-02-24'},
        {id: 3, title: '北京教育局', create_time: '2020-02-24'},
        {id: 1, title: '北京人力资源和社会保障局', create_time: '2020-02-24'},
        {id: 2, title: '北京卫健委', create_time: '2020-02-24'},
        {id: 3, title: '北京教育局', create_time: '2020-02-24'},
    ],
    '山东': [
        {id: 1, title: '山东人力资源和社会保障局', create_time: '2020-02-24'},
        {id: 2, title: '山东卫健委', create_time: '2020-02-24'},
        {id: 3, title: '山东教育局', create_time: '2020-02-24'},
        {id: 4, title: '山东司法局', create_time: '2020-02-24'},
    ],
    '湖北': [
        {id: 1, title: '湖北人力资源和社会保障局', create_time: '2020-02-24'},
        {id: 2, title: '湖北卫健委', create_time: '2020-02-24'},
        {id: 3, title: '湖北城市规划局', create_time: '2020-02-24'},
        {id: 4, title: '湖北水利水电局', create_time: '2020-02-24'},
    ],
    
}
// 全国省份列表
var dataMap = [{ name: '北京' }, { name: '天津' }, { name: '上海' }, { name: '重庆' }, { name: '河北' }, { name: '河南' }, { name: '云南' }, { name: '辽宁' }, { name: '黑龙江' }, { name: '湖南' }, { name: '安徽' }, { name: '山东' },
{ name: '新疆' }, { name: '江苏' }, { name: '浙江' }, { name: '江西' }, { name: '湖北' }, { name: '广西' }, { name: '甘肃' }, { name: '山西' }, { name: '内蒙古' }, { name: '陕西' }, { name: '吉林' }, { name: '福建' }, { name: '贵州' },
{ name: '广东' }, { name: '青海' }, { name: '西藏' }, { name: '四川' }, { name: '宁夏' }, { name: '海南' }, { name: '台湾' }, { name: '香港' }, { name: '澳门' }]
// 需要在页面上直接标记出来的城市
var specialMap = ['山东'];
// 对dataMap进行处理，使其可以直接在页面上展示
for (var i = 0; i < specialMap.length; i++) {
    for (var j = 0; j < dataMap.length; j++) {
        if (specialMap[i] == dataMap[j].name) {
            dataMap[j].selected = true;
            break;
        }
    }
}
// 绘制图表，准备数据
var option = {
    // tooltip: {
    //     formatter: function (params) {
            
    //         var info = 
    //         "<p style='font-size:18px; background-color:#fcb11d;'>"
    //         + params.name
            
    //         + '</p><p style="font-size:14px colro:#fcb11d">这里可以写一些，想展示在页面上的别的信息</p>'
    //         return info;
    //     },
    //     backgroundColor: "#ffffff",//提示标签背景颜色
    //     textStyle: { color: "#fff" }, //提示标签字体颜色
        
    // },
    series: [
        {
            name: '中国',
            type: 'map',
            mapType: 'china',
            selectedMode: 'single',
            label: {
                normal: {
                    show: true,//显示省份标签
                    // textStyle:{color:"#c71585"}//省份标签字体颜色
                },
                emphasis: {
                    show: true,//对应的鼠标悬浮效果
                    // textStyle:{color:"#800080"}
                }
            },
            itemStyle: {
                normal: {
                    borderWidth: .5,//区域边框宽度
                    borderColor: '#009fe8',//区域边框颜色
                    areaColor:"#ffefd5",//区域颜色
                },
                emphasis: {
                    borderWidth: .5,
                    borderColor: '#4b0082',
                    areaColor: "#ffdead",
                }
            },
            data: dataMap
        }
    ]
};
//初始化echarts实例
var myChart = echarts.init(document.getElementById('container'));
//使用制定的配置项和数据显示图表
myChart.setOption(option);
updataList ('山东')
// 设置图标点击事件 - 点击省份请求数据信息
myChart.on('click', function(params) {
    let provinceName = params.name;
    updataList (provinceName)
});

// 更新列表
function updataList (provinceName) {
    var list = dataSource[provinceName]
    document.getElementById('province').innerHTML = provinceName;
    var listSource = document.getElementById('listSource')
    var listNum = document.getElementById('listNum')
    if (list&&list.length > 0) {
        var num = list.length
        if (num > 8) {
            list = list.slice(0, 8)
        }
        var str = ''
        for (var i = 0; i < list.length; i++) {
            str += `<div class="item">
                        <div class="tit">${list[i].title}</div>
                    </div>`
        }
        if (num > 8) {
            str += `<div class="item">
                        <div class="tit">......</div>
                    </div>`
        }
        listSource.innerHTML = str
        listNum.innerHTML = num
    } else {
        listSource.innerHTML = `<div class="nodata">无数据</div>`
        listNum.innerHTML = 0
    }
}