        var mySwiper = new Swiper('.swiper-container1', {
            direction: 'horizontal', // 垂直切换选项
            loop: true, // 循环模式选项

            // 如果需要分页器
            pagination: {
                el: '.swiper-pagination',
            },

            // 如果需要前进后退按钮
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            autoplay: {
                delay: 8000,
            },
        });
        var mySwiper2 = new Swiper('.swiper-container2', {
            direction: 'horizontal', // 垂直切换选项
            loop: true, // 循环模式选项

            // 如果需要分页器
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            

            // 如果需要前进后退按钮
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            autoplay: {
                delay: 1115000,
            },
        });
        var swiper3 = new Swiper('.swiper-container3', {
            slidesPerView: 4,
            loop: true,
            slidesPerGroup: 1,
            spaceBetween: 30,
            autoplay: {
                delay: 3000,
            }
        });
